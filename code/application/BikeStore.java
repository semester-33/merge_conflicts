package application;
import vehicles.Bicycle;

//Uyen Dinh Michelle Banh 2234181

public class BikeStore {
    public static void main(String[] args){
        Bicycle[] b = new Bicycle[4];

        b[0] = new Bicycle("Michelle", 90, 500000);
        b[1] = new Bicycle("Swetha", 20, 50);
        b[2] = new Bicycle("Rida", 1, 0.000002);
        b[3] = new Bicycle("PeppaPig", 50, 100000000);
        
        for(int i = 0; i < b.length; i++){
            System.out.println(b[i]);
        }
    }
}
